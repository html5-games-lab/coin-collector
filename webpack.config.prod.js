'use strict'

const path = require('path')
const webpack = require('webpack')
const mergeConfigs = require('webpack-merge')
const commonConfig = require('./webpack.config.base')

const CleanWebpackPlugin = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = function (env) {
  return mergeConfigs(commonConfig(), {
    plugins: [
      new CleanWebpackPlugin(['dist/*.js', 'dist/*.html', 'dist/assets/*'], {
        root: path.resolve(__dirname),
        watch: true
      }),
      new CopyWebpackPlugin([
        { from: 'assets', to: 'assets' }
      ]),
      new webpack.optimize.UglifyJsPlugin({
        minimize: true,
        compress: {
          warnings: false,
          screw_ie8: true,
          dead_code: true,
          drop_console: true
        },
        comments: false,
        sourceMap: true
      })
    ]
  })
}
