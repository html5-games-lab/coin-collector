'use strict'

const webpack = require('webpack')
const path = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const OptimizeJSPlugin = require('optimize-js-plugin')

const phaserModule = path.join(__dirname, '/node_modules/phaser-ce/')
const phaser = path.join(phaserModule, 'build/custom/phaser-split.min.js')
const pixi = path.join(phaserModule, 'build/custom/pixi.min.js')
const p2 = path.join(phaserModule, 'build/custom/p2.min.js')
const config = path.join(__dirname, 'src/config.js')
const boot = path.join(__dirname, 'src/boot.js')
const load = path.join(__dirname, 'src/load.js')
const menu = path.join(__dirname, 'src/menu.js')
const play = path.join(__dirname, 'src/play.js')

module.exports = function () {
  return {
    devtool: 'cheap-module-source-map',
    context: path.resolve(__dirname),
    resolve: {
      modules: ['src', 'node_modules'],
      alias: {
        pixi: pixi,
        phaser: phaser,
        p2: p2,
        config$: config,
        boot$: boot,
        load$: load,
        menu$: menu,
        play$: play
      }
    },
    entry: {
      main: ['babel-polyfill', './src/index.js'],
      vendor: ['pixi', 'p2', 'phaser', 'webfontloader']
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      publicPath: '',
      filename: '[name].[hash].js'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: ['babel-loader?cacheDirectory=true']
        },
        {
          test: /pixi(\.min)?\.js/,
          use: ['expose-loader?PIXI']
        },
        {
          test: /phaser-split(\.min)?\.js/,
          use: ['expose-loader?Phaser']
        },
        {
          test: /p2(\.min)?\.js/,
          use: ['expose-loader?p2']
        },
        {
          test: /\.(mp3|ogg)$/i,
          use: ['file-loader?assets/[path][name].[ext]']
        },
        {
          test: /\.(jpe?g|png|gif)$/i,
          use: [
            { loader: 'file-loader?assets/[path][name].[ext]' },
            {
              loader: 'image-webpack-loader',
              options: {
                mozjpeg: {
                  progressive: true
                },
                gifsicle: {
                  interlaced: true
                },
                optipng: {
                  optimizationLevel: 7
                }
              }
            }
          ]
        }
      ]
    },
    plugins: [
      new webpack.LoaderOptionsPlugin({
        debug: true
      }),
      new webpack.EnvironmentPlugin({
        NODE_ENV: 'development',
        DEBUG: false
      }),
      new HtmlWebpackPlugin({
        title: '💰 Coin Collector! 💰',
        filename: 'index.html',
        template: 'src/index.ejs',
        hash: true,
        excludeChunks: 'appcache/manifest.appcache',
        minify: {
          collapseWhiteSpace: true,
          conservativeCollapse: true,
          html5: true,
          useShortDoctype: true,
          minifyCSS: true,
          minifyJS: true,
          minifyURLs: true
        }
      }),
      new webpack.optimize.CommonsChunkPlugin({
        names: ['vendor', 'extra', 'manifest'],
        minChunks: 3
      }),
      new OptimizeJSPlugin({
        sourceMap: true
      })
    ]
  }
}
