'use strict'

// eslint-disable-next-line no-unused-vars
const webpack = require('webpack')
const mergeConfigs = require('webpack-merge')
const commonConfig = require('./webpack.config.base')

const DashboardPlugin = require('webpack-dashboard/plugin')

module.exports = function (env) {
  return mergeConfigs(commonConfig(), {
    devServer: {
      compress: true
    },
    plugins: [
      new DashboardPlugin()
    ]
  })
}
