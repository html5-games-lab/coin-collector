import Phaser from 'phaser'

export const gameConfig = {
  nextState: 'boot',
  assetsPath: 'assets',
  width: 500,
  height: 340,
  renderer: Phaser.AUTO,
  parent: '',
  state: null,
  transparent: false,
  antialias: true,
  physicsConfig: Phaser.Physics.ARCADE,
  initialGlobal: { score: 0 },
  scoreLabelText: 'Score: '
}

export const bootConfig = {
  nextState: 'load',
  assets: {
    path: gameConfig.assetsPath,
    images: {
      filetype: 'png',
      player: 'player',
      progress: 'progressBar'
    }
  },
  backgroundColor: '#000',
  roundPixels: true
}

export const loadConfig = {
  nextState: 'menu',
  labels: {
    loading: {
      text: 'Loading...',
      textStyle: {
        font: '30px Geo',
        fill: '#fff'
      }
    }
  },
  assets: {
    path: gameConfig.assetsPath,
    tilemap: {
      name: 'map',
      filetype: 'json'
    },
    images: {
      progress: 'progressBar',
      player: 'player',
      mute: 'muteButton',
      filetype: 'png',
      names: [
        'coin',
        'enemy',
        'background',
        'pixel',
        'jumpButton',
        'leftButton',
        'rightButton',
        'tileset'
      ]
    },
    audio: {
      names: [
        'music',
        'jump',
        'coin',
        'dead'
      ],
      filetypes: ['mp3', 'ogg']
    }
  }
}

export const menuConfig = {
  nextState: 'play',
  assets: {
    images: {
      bg: 'background'
    }
  },
  labels: {
    gameTitle: {
      text: '💰 Coin Collector! 💰',
      textStyle: {
        font: '50px Geo',
        fill: '#fff'
      }
    },
    scoreLabel: {
      text: gameConfig.scoreLabelText,
      textStyle: {
        font: '25px Geo',
        fill: '#fff',
        align: 'center'
      }
    },
    startLabel: {
      text: 'Press ⬆️ to start',
      textStyle: {
        font: '25px Geo',
        fill: '#fff'
      }
    },
    bestScoreLabel: {
      text: 'Best Score: '
    }
  }
}

export const playConfig = {
  nextState: 'menu',
  labels: {
    scoreLabel: {
      text: gameConfig.scoreLabelText,
      textStyle: {
        font: '18px Geo',
        fill: '#fff'
      }
    }
  },
  assets: {
    images: {
      player: 'player',
      coin: 'coin',
      enemy: 'enemy',
      pixel: 'pixel'
    },
    audio: {
      jump: 'jump',
      coin: 'coin',
      dead: 'dead'
    }
  }
}
