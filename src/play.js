import Phaser from 'phaser'

import { playConfig } from 'config'

const Play = (game) => {
  const images = playConfig.assets.images
  const audio = playConfig.assets.audio
  const localStorage = window.localStorage

  const movePlayer = () => {
    const player = game.global.player
    const cursor = game.global.cursor
    const wasd = game.global.wasd

    if (cursor.left.isDown || wasd.left.isDown) {
      player.body.velocity.x = -200
      player.animations.play('left')
    } else if (cursor.right.isDown || wasd.right.isDown) {
      player.body.velocity.x = 200
      player.animations.play('right')
    } else {
      player.body.velocity.x = 0
      player.animations.stop()
      player.frame = 0
    }

    if ((cursor.up.isDown || wasd.up.isDown) &&
      player.body.onFloor()) {
      game.global.jumpSound.play()
      player.body.velocity.y = -320
    }
  }

  const setCoinPosition = () => {
    let coinPosition = [
      { x: 140, y: 68 },
      { x: 360, y: 68 },
      { x: 60, y: 148 },
      { x: 440, y: 148 },
      { x: 130, y: 308 },
      { x: 370, y: 308 }
    ]

    coinPosition.forEach((position) => {
      if (position.x === game.global.coin.x) {
        coinPosition.splice(coinPosition.indexOf(position), 1)
      }
    })

    const newPosition = game.rnd.pick(coinPosition)
    game.global.coin.reset(newPosition.x, newPosition.y)
  }

  const takeCoin = () => {
    game.global.coinSound.play()
    game.global.coin.scale.setTo(0, 0)

    game.add.tween(game.global.coin.scale)
      .to({ x: 1.5, y: 1.5 }, 100) // go from 0 to original scale in .1 seconds
      .to({ x: 1, y: 1 }, 100)
      .start()

    game.add.tween(game.global.player.scale)
      .to({ x: 1.5, y: 1.5 }, 100) // scale the player up a bit
      .yoyo(true) // reverses the tween
      .start()

    game.global.score += 5
    game.global.scoreLabel.text = `${playConfig.labels.scoreLabel.text}${game.global.score}`
    setCoinPosition()
  }

  const playerDie = () => {
    const player = game.global.player
    const emitter = game.global.emitter

    if (!player.alive) return

    player.kill()
    game.global.deadSound.play()
    game.camera.shake(0.02, 300)

    emitter.x = player.x
    emitter.y = player.y
    emitter.start(true, 1500, null, 30)

    if (game.global.score > localStorage.getItem('bestScore')) {
      localStorage.setItem('bestScore', game.global.score)
    }

    game.global.music.stop()
    game.time.events.add(1000, () => game.state.start(playConfig.nextState))
  }

  const addEnemy = () => {
    const enemy = game.global.enemies.getFirstDead()
    if (!enemy) return

    enemy.anchor.setTo(0.5, 1)
    enemy.reset(game.width / 2, 0)
    enemy.body.gravity.y = 500
    enemy.body.velocity.x = 100 * game.rnd.pick([-1, 1])
    enemy.body.bounce.x = 1
    enemy.checkWorldBounds = true
    enemy.outOfBoundsKill = true
  }

  const createIO = () => {
    game.global.cursor = game.input.keyboard.createCursorKeys()
    game.global.jumpSound = game.add.audio(audio.jump)
    game.global.coinSound = game.add.audio(audio.coin)
    game.global.deadSound = game.add.audio(audio.dead)

    game.input.keyboard.addKeyCapture(
      [Phaser.Keyboard.UP, Phaser.Keyboard.DOWN,
        Phaser.Keyboard.LEFT, Phaser.Keyboard.RIGHT]
    )

    game.global.wasd = {
      up: game.input.keyboard.addKey(Phaser.Keyboard.W),
      left: game.input.keyboard.addKey(Phaser.Keyboard.A),
      right: game.input.keyboard.addKey(Phaser.Keyboard.D)
    }
  }

  const createScore = () => {
    game.global.score = 0
    game.global.scoreLabel = game.add.text(30, 30, `${playConfig.labels.scoreLabel.text}${game.global.score}`,
      playConfig.labels.scoreLabel.textStyle)
  }

  const createPlayer = () => {
    const player = game.global.player = game.add.sprite(game.width / 2, game.height / 2, images.player)
    player.anchor.setTo(0.5, 0.5)
    game.physics.arcade.enable(player)
    player.body.gravity.y = 500
    player.animations.add('right', [1, 2], 8, true)
    player.animations.add('left', [3, 4], 8, true)
  }

  const createCoin = () => {
    const coin = game.global.coin = game.add.sprite(60, 148, images.coin)
    game.physics.arcade.enable(coin)
    coin.anchor.setTo(0.5, 0.5)
  }

  const createEnemies = () => {
    const enemies = game.global.enemies = game.add.group()

    enemies.enableBody = true
    enemies.createMultiple(10, images.enemy)

    game.global.nextEnemy = 0
  }

  const createParticles = () => {
    const emitter = game.global.emitter = game.add.emitter(0, 0, 30)
    emitter.makeParticles(images.pixel)
    emitter.setYSpeed(-150, 150)
    emitter.setXSpeed(-150, 150)
    emitter.setScale(2, 0, 2, 0, 1000)
    emitter.gravity.set(0, 0)
  }

  const createWorld = () => {
    const map = game.add.tilemap('map')
    map.addTilesetImage('tileset')
    const layer = game.global.layer = map.createLayer('Tile Layer 1')
    layer.resizeWorld()
    map.setCollision(1)
  }

  const updateCollisions = () => {
    game.physics.arcade.collide(game.global.player, game.global.layer)
    game.physics.arcade.collide(game.global.enemies, game.global.layer)
  }

  const updateOverlaps = () => {
    game.physics.arcade.overlap(game.global.player, game.global.coin, takeCoin, null)
    game.physics.arcade.overlap(game.global.player, game.global.enemies, playerDie, null)
  }

  const updatePlayer = () => {
    if (!game.global.player.inWorld) playerDie()

    movePlayer()
  }

  const updateEnemies = () => {
    if (game.global.nextEnemy < game.time.now) {
      const start = 5000 // start at 1 enemy every 5 seconds
      const end = 1000 // start at 1 enemy every seconds
      const score = 100 // reach the end of the progression by 100 points
      const delay = Math.max(start - (start - end) * game.global.score / score, end)

      addEnemy()
      game.global.nextEnemy = game.time.now + delay
    }
  }

  return {
    create: () => {
      [createIO,
        createScore,
        createPlayer,
        createCoin,
        createEnemies,
        createParticles,
        createWorld].forEach((f) => f.call())
    },
    update: () => {
      [updateCollisions,
        updateOverlaps,
        updatePlayer,
        updateEnemies].forEach((f) => f.call())
    }
  }
}

export default Play
