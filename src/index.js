import Phaser from 'phaser'

import bootState from 'boot'
import loadState from 'load'
import menuState from 'menu'
import playState from 'play'

import { gameConfig } from 'config'

const Start = (gameConfig) => {
  const {
    width,
    height,
    renderer,
    parent,
    state,
    transparent,
    antialias,
    physicsConfig,
    initialGlobal,
    nextState } = gameConfig

  const game = new Phaser.Game(width, height, renderer, parent, state,
    transparent, antialias, physicsConfig)

  game.global = initialGlobal
  game.state.add('boot', bootState(game))
  game.state.add('load', loadState(game))
  game.state.add('menu', menuState(game))
  game.state.add('play', playState(game))

  game.state.start(nextState)
}

Start(gameConfig)
