import Phaser from 'phaser'

import { loadConfig } from 'config'

const Load = (game) => {
  const images = loadConfig.assets.images
  const audio = loadConfig.assets.audio

  const preloadLoadingLabel = () => {
    const labelConfig = loadConfig.labels.loading

    const loadingLabel = game.add.text(game.width / 2, 150, labelConfig.text,
      labelConfig.textStyle)

    loadingLabel.anchor.setTo(0.5, 0.5)
  }

  const preloadProgressSprite = () => {
    const progressBar = game.add.sprite(game.width / 2, 200, images.progress)
    progressBar.anchor.setTo(0.5, 0.5)
    game.load.setPreloadSprite(progressBar)
  }

  const preloadPlayerSpritesheet = () => {
    game.load.spritesheet(images.player,
      `${loadConfig.assets.path}/${images.player}.${images.filetype}`, 20, 20)
  }

  const preloadMuteSpritesheet = () => {
    game.load.spritesheet(images.mute,
      `${loadConfig.assets.path}/${images.mute}.${images.filetype}`, 28, 22)
  }

  const preloadImageAssets = () => {
    images.names.forEach((name) => {
      game.load.image(name, `${loadConfig.assets.path}/${name}.${images.filetype}`)
    })
  }

  const preloadAudioAssets = () => {
    audio.names.forEach((name) => {
      game.load.audio(name, audio.filetypes.map((filetype) => {
        return `${loadConfig.assets.path}/${name}.${filetype}`
      }))
    })
  }

  const preloadTilemap = () => {
    game.load.tilemap(loadConfig.assets.tilemap.name,
      `${loadConfig.assets.path}/${loadConfig.assets.tilemap.name}.${loadConfig.assets.tilemap.filetype}`,
      null, Phaser.Tilemap.TILED_JSON)
  }

  return {
    preload: () => {
      [preloadLoadingLabel,
        preloadProgressSprite,
        preloadPlayerSpritesheet,
        preloadMuteSpritesheet,
        preloadImageAssets,
        preloadAudioAssets,
        preloadTilemap].forEach((f) => f.call())
    },
    create: () => game.state.start(loadConfig.nextState)
  }
}

export default Load
