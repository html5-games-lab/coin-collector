import Phaser from 'phaser'

import { menuConfig } from 'config'

const Menu = (game) => {
  const images = menuConfig.assets.images
  const localStorage = window.localStorage

  const toggleSound = () => {
    game.sound.mute = !game.sound.mute

    game.global.muteButton.frame = game.sound.mute ? 1 : 0
  }

  const createBackground = () => game.add.image(0, 0, images.bg)

  const createTitle = () => {
    const title = game.add.text(game.width / 2, -50, menuConfig.labels.gameTitle.text,
      menuConfig.labels.gameTitle.textStyle)

    game.add.tween(title)
      .to({ y: 80 }, 1000)
      .easing(Phaser.Easing.Bounce.Out)
      .start()

    title.anchor.setTo(0.5, 0.5)
  }

  const createScoreLabel = () => {
    const bestScore = localStorage.getItem('bestScore')

    const labelText = `${menuConfig.labels.scoreLabel.text}${game.global.score}\n${menuConfig.labels.bestScoreLabel.text}${bestScore}`

    const scoreLabel = game.add.text(game.width / 2, game.height / 2, labelText,
      menuConfig.labels.scoreLabel.textStyle)

    scoreLabel.anchor.setTo(0.5, 0.5)
  }

  const createStartLabel = () => {
    const startLabel = game.add.text(game.width / 2, game.height - 80,
      menuConfig.labels.startLabel.text, menuConfig.labels.startLabel.textStyle)

    game.add.tween(startLabel)
      .to({ angle: -2 }, 500)
      .to({ angle: 2 }, 1000)
      .to({ angle: 0 }, 500)
      .loop()
      .start()

    startLabel.anchor.setTo(0.5, 0.5)
  }

  const createMuteButton = () => {
    game.global.muteButton = game.add.button(20, 20, 'muteButton', toggleSound)
    game.global.muteButton.frame = game.sound.mute ? 1 : 0
  }

  const createKeyBindings = () => {
    const upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP)
    upKey.onDown.add(() => game.state.start(menuConfig.nextState))
  }

  const createMusic = () => {
    const music = game.global.music = game.add.audio('music')
    music.loop = true
    music.volume = 0.25
    music.play()
  }

  const createBestScore = () => {
    if (!localStorage.getItem('bestScore')) {
      localStorage.setItem('bestScore', 0)
    }
  }

  return {
    create: () => {
      [createBackground,
        createTitle,
        createScoreLabel,
        createStartLabel,
        createMuteButton,
        createKeyBindings,
        createMusic,
        createBestScore].forEach((f) => f.call())
    }
  }
}

export default Menu
