import Phaser from 'phaser'

import { bootConfig } from 'config'

const Boot = (game) => {
  const createScaling = () => {
    if (!game.device.desktop) {
      game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL

      game.scale.setMinMax(game.width / 2, game.height / 2, game.width * 2, game.height * 2)

      game.scale.pageAlignHorizontally = true
      game.scale.pageAlignVertically = true

      document.body.style.backgroundColor = bootConfig.backgroundColor
    }
  }

  return {
    preload: () => {
      const images = bootConfig.assets.images
      game.load.image(images.progress,
        `${bootConfig.assets.path}/${images.progress}.${images.filetype}`)
    },
    create: () => {
      game.stage.backgroundColor = bootConfig.backgroundColor
      game.renderer.renderSession.roundPixels = bootConfig.roundPixels
      createScaling()
      game.state.start(bootConfig.nextState)
    }
  }
}

export default Boot
